<?php

$var = ' ';

// This will evaluate to TRUE so the tex will be printed.
if (isset($var)){
    echo "This var is set so I Will Print.";
}

// In the next examples we'll use var_dump to Output
// the return value of isset().
$a = "test";
$b = "anothertest";
var_dump(isset($a));          // True
var_dump(isset($a, $b));   //True

unset ($a);

var_dump(isset($a));          //False
var_dump(isset($a, $b));    //False


$foo = NULL;
var_dump(isset($foo));     // False


?>

