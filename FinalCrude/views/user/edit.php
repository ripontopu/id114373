<?php

error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../vendor/autoload.php';

use App\user\user;


session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$id = $_GET['id'];

$newedit = new User();

$edititem = $newedit->show($id);

?>


<a href="create.php">Create User</a> | <a href="index.php">Back To List</a>


<fieldset>
    <legend>Update YOur Name</legend>
    <form action="update.php" method="POST">
        <input type="text" name="title" id="title" value="<?php echo $edititem['title']; ?>"></br></br>
        <input type="submit" value="Update">
        <input type="hidden" name="id" value="<?php echo $id; ?>">
    </form>
</fieldset>