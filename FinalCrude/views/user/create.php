<?php
session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

include_once '../../vendor/autoload.php';

use App\user\user;
?>

<fieldset>
    <legend>User Form</legend>
    <form action="store.php" method="POST">
        <input type="text" name="title" id="title"></br></br>
        <input type="submit" value="Save" id="save">
        <input type="submit" value="Save & Add" id="saveadd">
        <input type="reset" value="Reset" id="reset">
    </form>
</fieldset>
