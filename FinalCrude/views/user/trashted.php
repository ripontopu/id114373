<?php
error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../vendor/autoload.php';

use App\user\user;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$showdelete = new User();

$Alldelete = $showdelete->trashted();



?>


<a href="create.php">Create User</a>  |  <a href="index.php">Show all Data</a>

<table border="1">
    <tr>
        <th>
            SL
        </th>
        <th>
            Title
        </th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($Alldelete) && !empty($Alldelete)) {
        $serial = 0;
        foreach ($Alldelete as $deleteitem) {
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td><?php echo $deleteitem['title'] ?></td>                 
                <td>
                    <a href="show.php?id=<?php echo $deleteitem['id'] ?>">Show Deails</a>  |
                    <a href="edit.php?id=<?php echo $deleteitem['id'] ?>">Edit</a>  |
                    <a href="delete.php?id=<?php echo $deleteitem['id'] ?>">Delete Permanently</a> 


                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="3">
                <?php echo "No avilable Data"; ?>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
