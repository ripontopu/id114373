<?php 

include_once '../../../vendor/autoload.php';
use App\id114373\Users\Users;


$showall = new Users();

$allitem = $showall->index();

?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
              <title>Mini Project</title>

              <!-- Bootstrap -->
              <link href="../../../css/bootstrap.min.css" rel="stylesheet">

              <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
              <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
              <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->
       </head>
       <body>
              <div class="header_area">
                     <div class="container">
                            <div class="row">
                                   <nav class="navbar navbar-default">
                                          <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                 </button>
                                                 <a class="navbar-brand" href="#">Mini Project</a>
                                          </div>

                                          <!-- Collect the nav links, forms, and other content for toggling -->
                                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                                 <ul class="nav navbar-nav">
                                                        <li><a href="index.php">Home</a></li>
                                                        <li><a href="alluser.php">all User List</a></li>
                                                        <li><a href="allactive.php">Active User List</a></li>
                                                        <li><a href="#">Login</a></li>
                                                        <li><a class="active" href="Signup.php">Sign Up</a></li>
                                                 </ul>
                                          </div><!-- /.navbar-collapse -->
                                        
                                   </nav>
                            </div>
                     </div>
              </div>
              <div class="slider_area"></div>
              <div class="content">

                <div class="container">
                      
                    <div class="row">
                        <table class="table">
                            <thead class="thead-inverse">
                              <tr>
                                <th>Id</th>
                                <th>User Name</th>
                                <th>Email Address</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php 
        if(isset($allitem) && !empty($allitem)){
            $serial = 0;
            foreach ($allitem as $oneitem){
                $serial++
               ?>   
                              <tr>
                                <th scope="row"><?php echo $serial; ?></th>
                                <td><?php echo $oneitem['username']; ?></td>
                                 <td><?php echo $oneitem['email']; ?></td>
                                 <td>
                                <a href="delete.php?id=<?php echo $oneitem['id'] ?>">Delete</a> | 
                                <a href="active.php?id=<?php echo $oneitem['id'] ?>">Activate</a> 
                            
                                 </td>
                              
                            </tbody>
                              <?php
            }
        }
        else{
       ?>
       <tr>
            <td colspan="3">
                <?php echo "No avilable Data"; ?>
            </td>
        </tr>
        <?php
        }
        ?>
                          </table>
                    </div>
                </div>
              <div class="footer_area">
                     <div class="container">
                            <div class="row">

                            </div>
                     </div>
              </div>
              <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <!-- Include all compiled plugins (below), or include individual files as needed -->
              <script src="../../../js/bootstrap.min.js"></script>
       </body>
</html>