<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitb47a0f5a59e479873ce1cf24128bcf96
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitb47a0f5a59e479873ce1cf24128bcf96::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitb47a0f5a59e479873ce1cf24128bcf96::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
