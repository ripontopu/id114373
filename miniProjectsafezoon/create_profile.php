<?php


?>


<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <title>Mini Project</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to impro                ve your experience.</p>
        <![endif]-->
         <div class="header_area">
                     <div class="container">
                            <div class="row">
                                   <nav class="navbar navbar-default">
                                          <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                 </button>
                                                 <a class="navbar-brand" href="#">Mini Project</a>
                                          </div>

                                          <!-- Collect the nav links, forms, and other content for toggling -->
                                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                                 <ul class="nav navbar-nav">
                                                        <li><a href="index.php">Home</a></li>
                                                        <li><a href="alluser.php">all User List</a></li>
                                                        <li><a href="allactive.php">Active User List</a></li>
                                                        <li><a href="Login.php">Login</a></li>
                                                        <li><a class="active" href="../../../Signup.php">Sign Up</a></li>
                                                 </ul>
                                          </div><!-- /.navbar-collapse -->
                                   </nav>
                            </div>
                     </div>
              </div>
              <div class="slider_area"></div>
<div class="container">
    <h1>Create Your Profile</h1>
  	<hr>
	<div class="row">
      <!-- left column -->
     
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        <div class="alert alert-info alert-dismissable">
          <a class="panel-close close" data-dismiss="alert">×</a> 
          <i class="fa fa-coffee"></i>
          This is an <strong>.alert</strong>. Use this to show important messages to the user.
        </div>
        <h3>Personal info</h3>
        
        <form class="form-horizontal" action="Views/id114373/Users/store.php" role="form" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label class="col-lg-3 control-label">First name:</label>
            <div class="col-lg-8">
                <input class="form-control" type="text" name="f_name" value="Ripon">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Last name:</label>
            <div class="col-lg-8">
                <input class="form-control" type="text" name="l_name" value="Topu">
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-lg-3 control-label">Personal Phone:</label>
            <div class="col-lg-8">
                <input class="form-control" type="text" name="personal_phone" value="000-0000-00000">
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-lg-3 control-label">Home Phone:</label>
            <div class="col-lg-8">
                <input class="form-control" type="text" name="home_phone" value="000-0000-00000">
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-lg-3 control-label">Office Phone:</label>
            <div class="col-lg-8">
                <input class="form-control" type="text" name="office_phone" value="000-0000-00000">
            </div>
          </div>
             <div class="form-group">
                 <label class="col-lg-3 control-label"> Upload Your Photo</label>
                 <div class="col-lg-8">
                     <input type="file" class="form-control" name="image">
                 </div>
             </div>
             
          <div class="form-group">
            <label class="col-lg-3 control-label">Present Address:</label>
            <div class="col-lg-8">
                <textarea cols="50" rows="5" name="currentAddress"></textarea>
            </div>
          </div>
            
          <div class="form-group">
            <label class="col-lg-3 control-label">Permanent Address:</label>
            <div class="col-lg-8">
                 <textarea cols="50" rows="5" name="permanentAddress"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
                <input class="form-control" type="email" name="email" value="abc@web.com">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Time Zone:</label>
            <div class="col-lg-8">
              <div class="ui-select">
                <select id="user_time_zone" class="form-control">
                  <option value="Hawaii">(GMT-10:00) Hawaii</option>
                  <option value="Alaska">(GMT-09:00) Alaska</option>
                  <option value="Pacific Time (US &amp; Canada)">(GMT-08:00) Pacific Time (US &amp; Canada)</option>
                  <option value="Arizona">(GMT-07:00) Arizona</option>
                  <option value="Mountain Time (US &amp; Canada)">(GMT-07:00) Mountain Time (US &amp; Canada)</option>
                  <option value="Central Time (US &amp; Canada)" selected="selected">(GMT-06:00) Central Time (US &amp; Canada)</option>
                  <option value="Eastern Time (US &amp; Canada)">(GMT-05:00) Eastern Time (US &amp; Canada)</option>
                  <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                </select>
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
                <input type="submit" class="btn btn-primary" value="Save Changes">
              <span></span>
              <input type="reset" class="btn btn-default" value="Cancel">
            </div>
          </div>
        </form>
      </div>
  </div>
</div>
<hr>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
