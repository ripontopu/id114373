<?php

namespace App\id114373\Profile;
use PDO;
class Profile {

    public $id='';
    public $user_name='root';
    public $connect= '';
    public $f_name='';
    public $l_name='';
    public $user_group_id='';
    public $unique_id='';
    public $username='';
    public $password='';
    public $email='';
    public $modified_at='';
    public $created_at='';
    public $delete_at='';
    public $is_active='';
    public $currentAddress='';
    public $permanentAddress='';
    public $personal_phone='';
    public $home_Phone='';
    public $office_phone='';
    public $image_name='';

    public function __construct() {
        
        try {
            $this->connect = new PDO ("mysql:host=localhost; dbname=usrreg",$this->user_name, $this->password);
            $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }


       // $connect = mysql_connect('localhost','root')or die('Serv Not Found');
      // mysql_select_db('usrreg') or die('Database Not Found');
    }

    public function prepare($data = '') {
       if(array_key_exists('image', $data)){
            $this->image_name = $data['image'];
        }
        if(array_key_exists('f_name', $data)){
            $this->f_name = $data['f_name'];
        }
        if(array_key_exists('l_name', $data)){
            $this->l_name = $data['l_name'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('personal_phone', $data)){
            $this->personal_phone = $data['personal_phone'];
        }
        if(array_key_exists('home_Phone', $data)){
            $this->home_Phone = $data['home_Phone'];
        }
        if(array_key_exists('office_phone', $data)){
            $this->office_phone = $data['office_phone'];
        }
        if(array_key_exists('currentAddress', $data)){
            $this->currentAddress = $data['currentAddress'];
        }
        if(array_key_exists('permanentAddress', $data)){
            $this->permanentAddress = $data['permanentAddress'];
        }
        if(array_key_exists('unique_id', $data)){
            $this->unique_id = $data['unique_id'];
        }
        if(array_key_exists('modified_at', $data)){
            $this->modified_at = $data['modified_at'];
        }
        
        if(array_key_exists('created_at', $data)){
            $this->created_at = $data['created_at'];
        }
        
        if(array_key_exists('delete_at', $data)){
            $this->delete_at = $data['delete_at'];
        }
        
        if(array_key_exists('username', $data)){
            $this->username = $data['username'];
        }
        
        if(array_key_exists('password', $data)){
            $this->password = $data['password'];
        }
        if(array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('user_group_id', $data)){
            $this->user_group_id = $data['user_group_id'];
        }
        
        if(array_key_exists('is_active', $data)){
            $this->is_active = $data['is_active'];
        }
        return $this;
    }
    
    
    public function store() {
        session_start();
    $query2 == "INSERT INTO `profiles` (`id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `current_address`, `profile_pic`, `permanent_address`) "
            . "VALUES (NULL, :f_name, :l_name, :p_phone, :h_phone, :o_phone, :p_address, :photo, :c_address)";
    $storeid=$this->connect->prepare($query);
    $storeid->bindParam(":f_name", $this->username);
    $storeid->bindParam(":l_name", $this->username);
    $storeid->bindParam(":p_phone", $this->username);
    $storeid->bindParam(":h_phone", $this->username);
    $storeid->bindParam(":o_phone", $this->username);
    $storeid->bindParam(":p_address", $this->username);
    $storeid->bindParam(":photo", $this->image_name);
    $storeid->bindParam(":c_address", $this->username);
    $storeid->execute();
         $_SESSION['Message']="Uplode Successfully";
        header('location:index.php');
    }
    
    
    
    
    
    
}