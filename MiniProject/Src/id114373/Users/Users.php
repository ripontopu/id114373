<?php

namespace App\id114373\Users;
use PDO;
class Users {

    public $id='';
    public $user_name='root';
    public $connect= '';
    public $f_name='';
    public $l_name='';
    public $user_group_id='';
    public $unique_id='';
    public $username='';
    public $password='';
    public $email='';
    public $modified_at='';
    public $created_at='';
    public $delete_at='';
    public $is_active='';
    public $currentAddress='';
    public $permanentAddress='';
    public $personal_phone='';
    public $home_Phone='';
    public $office_phone='';
    public $image_name='';

    public function __construct() {
        
        try {
            $this->connect = new PDO ("mysql:host=localhost; dbname=usrreg",$this->user_name, $this->password);
            $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }


       // $connect = mysql_connect('localhost','root')or die('Serv Not Found');
      // mysql_select_db('usrreg') or die('Database Not Found');
    }

    public function prepare($data = '') {
       if(array_key_exists('image_name', $data)){
            $this->image_name = $data['image_name'];
        }
        if(array_key_exists('f_name', $data)){
            $this->f_name = $data['f_name'];
        }
        if(array_key_exists('l_name', $data)){
            $this->l_name = $data['l_name'];
        }
        if(array_key_exists('id', $data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('personal_phone', $data)){
            $this->personal_phone = $data['personal_phone'];
        }
        if(array_key_exists('home_Phone', $data)){
            $this->home_Phone = $data['home_Phone'];
        }
        if(array_key_exists('office_phone', $data)){
            $this->office_phone = $data['office_phone'];
        }
        if(array_key_exists('currentAddress', $data)){
            $this->currentAddress = $data['currentAddress'];
        }
        if(array_key_exists('permanentAddress', $data)){
            $this->permanentAddress = $data['permanentAddress'];
        }
        if(array_key_exists('unique_id', $data)){
            $this->unique_id = $data['unique_id'];
        }
        if(array_key_exists('modified_at', $data)){
            $this->modified_at = $data['modified_at'];
        }
        
        if(array_key_exists('created_at', $data)){
            $this->created_at = $data['created_at'];
        }
        
        if(array_key_exists('delete_at', $data)){
            $this->delete_at = $data['delete_at'];
        }
        
        if(array_key_exists('username', $data)){
            $this->username = $data['username'];
        }
        
        if(array_key_exists('password', $data)){
            $this->password = $data['password'];
        }
        if(array_key_exists('email', $data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('user_group_id', $data)){
            $this->user_group_id = $data['user_group_id'];
        }
        
        if(array_key_exists('is_active', $data)){
            $this->is_active = $data['is_active'];
        }
        return $this;
    }

    public function signup() {
        session_start();
             $query = "INSERT INTO `users` (`id`, `username`, `password`, `email`) VALUES (NULL,:name, :pass, :email);";
    $storeid=$this->connect->prepare($query);
    $storeid->bindParam(":name", $this->username);
    $storeid->bindParam(":pass", $this->password);
    $storeid->bindParam(":email", $this->email);
    $storeid->execute();
         $_SESSION['Message']="Registration Successfull";
         header('location:../../../signup.php');
        
  
    }
    
    
    public function login() {
        try {
            $query = "SELECT * FROM `users` WHERE username=:username and password=:password and `is_active` = '1'";
        
            $login_item = $this->connect->prepare($query)or die('Database Not Connected');
            $login_item->bindParam(":username",  $this->username);
            $login_item->bindParam(":password",  $this->password);
             $oneloginitem=$login_item->fetch(PDO::FETCH_ASSOC);
       
        if($rows==1 && $this->username=="admin" || $this->username=="admin@gmail.com")
            {   
                session_start();
                $_SESSION['username'] = "$this->username";
                $_SESSION['id'] = "$this->id";
                header("Location:../../../Views/id114373/Users/dashboard.php"); // Redirect user to index.php
            }
        elseif($rows==1)
            {   
                session_start();
                $_SESSION['username'] = "$this->username";
                $_SESSION['id'] = "$this->id";
                header("Location: ../../../Views/id114373/Users/Login.php"); // Redirect user to index.php
            }
        
        else 
            {
                $home_url = '../../../Views/id114373/Users/dashboard.php';
                header('Location: '.$home_url);
            }
   
        $_SESSION['loginname'] = $login_item['username'];
        $_SESSION['id'] = $login_item['id'];
        return $oneloginitem;
            
            
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
        
    }
    
    
    public function index() {
         try {
            $alluser = array();
        $query ="SELECT * FROM `users` WHERE is_active IS NULL";
        $allshow= $this->connect->query($query) or die("failed");
        while ($row = $allshow->fetch(PDO::FETCH_ASSOC)){
            $alluser[]=$row;
        }
        return $alluser;
            
        } catch (PDOException $exc) {
            echo $exc->getMessge();
        }
        }
    
    
    
    public function updateActive() {
        try {
             $query = "UPDATE `usrreg`.`users` SET  `is_active` = 1 WHERE `users`.`id` =" . $this->id;
       $activeitem= $this->connect->query($query) or die('Failed');
//        echo $query;
//        die();
        $_SESSION['Message'] = "<h2>" . "User Account Activated" . "</h2>";
        header('location:../../../Views/id114373/Users/alluser.php');
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }

       
   }
   
   public function active() {
        try {
            $alluser = array();
        $query ="SELECT * FROM `users` WHERE `is_active` = 1";
        $allshow= $this->connect->query($query) or die("failed");
        while ($row = $allshow->fetch(PDO::FETCH_ASSOC)){
            $alluser[]=$row;
        }
        return $alluser;
        header('location:../../../Views/id114373/Users/alluser.php');
            
        } catch (PDOException $exc) {
            echo $exc->getMessge();
        }
        
    }
    
    
    public function delete($id='') {
        try {
        $this->id=$id;
        $query="DELETE FROM `users` WHERE id=".$this->id;
//        echo $query;
//        die();
        $delteitem= $this->connect->query($query)or die('somethis worng');
        
         $_SESSION['Message']="<h2>Delete Successfull</h2>";
        header('location:../../../Views/id114373/Users/alluser.php');
    
      
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
        }
    
    
        
        
        
        
    public function store($id= "") {
        try {
            $query = "INSERT INTO `profiles` (`id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `current_address`, `profile_pic`, `permanent_address`) "
            . "VALUES (NULL, :f_name, :l_name, :p_phone, :h_phone, :o_phone, :p_address, :photo, :c_address)";
     
            $storeid=$this->connect->prepare($query);
    $storeid->bindParam(":f_name", $this->username);
    $storeid->bindParam(":l_name", $this->username);
    $storeid->bindParam(":p_phone", $this->username);
    $storeid->bindParam(":h_phone", $this->username);
    $storeid->bindParam(":o_phone", $this->username);
    $storeid->bindParam(":p_address", $this->username);
    $storeid->bindParam(":photo", $this->image_name);
    $storeid->bindParam(":c_address", $this->username);
   
         $_SESSION['Message']="Uplode Successfully";
        header('location:../../../Views/id114373/Users/dashboard.php');
            
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
                 
    }
      
    
    
    
    

}
