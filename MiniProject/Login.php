<?php 

include_once 'vendor/autoload.php';
use App\id114373\Users\Users;
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
              <title>Mini Project</title>

              <!-- Bootstrap -->
              <link href="css/bootstrap.min.css" rel="stylesheet">

              <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
              <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
              <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
              <![endif]-->
       </head>
       <body>
              <div class="header_area">
                     <div class="container">
                            <div class="row">
                                   <nav class="navbar navbar-default">
                                          <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                 </button>
                                                 <a class="navbar-brand" href="#">Mini Project</a>
                                          </div>

                                          <!-- Collect the nav links, forms, and other content for toggling -->
                                          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                                 <ul class="nav navbar-nav">
                                                        <li><a href="index.php">Home</a></li>
                                                        <li><a href="#">User</a>
                                                               <ul class="dropdown-menu">
                                                                    <li><a href="#">All User</a></li>
                                                                </ul>
                                                        </li>
                                                        <li><a href="#">Profile</a></li>
                                                        <li><a href="#">Login</a></li>
                                                        <li><a class="active" href="Signup.php">Sign Up</a></li>
                                                 </ul>
                                          </div><!-- /.navbar-collapse -->
                                   </nav>
                            </div>
                     </div>
              </div>
              <div class="slider_area"></div>
              <div class="content">


                     <div class="container">
                            <h1>Registration Form</h1>
                            <div class="row">
                                <div class="col-lg-5">
                                          <div class="col-md-12">
                                                 
                                               <?php
                                   session_start();

                                       if(isset($_SESSION['Message'])){
                                           ?>
                            
                                         <div class="alert alert-success">
                                    <strong><span class="glyphicon glyphicon-ok">
                                        <?php 
                                            echo $_SESSION['Message'];
                                        ?>
                                    </span> </strong>
                                              </div>
                                            <?php
                                            unset($_SESSION['Message']);
                                       }

                                   ?>  
                                          </div>
                                   </div>
                                <div class="col-lg-6 col-md-push-1"></div>
                            </div>
                                  
                      
                           
                            <div class="row">
                                   <form role="form" action="Views/id114373/Users/loging_process.php" method="POST">
                                          <div class="col-lg-6">
                                                 <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
                                                

                            <div class="form-group">
                                <label for="InputUsername">User Name Or Email</label>
                                <div class="input-group">
                                     <input name="usernameoremail" type="text"class="form-control" id="inputUsername" placeholder="User Name Or Email" required="">
                                     <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                                </div>

                             </div> 
                        <div class="form-group">
                            <label for="InputEmail">Password</label>
                            <div class="input-group">
                                   <input type="password" class="form-control" id="InputPassword" name="password" placeholder="Password" required>
                                   <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                            </div>
                        </div>
                                                
                                                 <input type="submit" name="submit" id="submit" value="Sign In" class="btn btn-info pull-right">
                                                  <div class="panel-footer">Not Registered? <a href="Signup.php" class="">Register here</a>
                                          </div>
                                      
                                   </form>
                                   <div class="col-lg-5 col-md-push-1">
                                          <div class="col-md-12">
                                                 
                                          </div>
                                   </div>
                            </div>
                     </div>


              </div>
              <div class="footer_area">
                     <div class="container">
                            <div class="row">

                            </div>
                     </div>
              </div>
              <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <!-- Include all compiled plugins (below), or include individual files as needed -->
              <script src="js/bootstrap.min.js"></script>
       </body>
</html>