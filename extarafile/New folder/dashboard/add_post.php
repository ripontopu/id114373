<?php
include_once('index.php');
?>
<div class="container-fluid">
    <div class="row">
        <?php include_once('sidebar.php'); ?>
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="dashboard">
                    <div class="page-header"><h3>New Post</h3></div>
                    <form action="control/newpost.php" method="POST">
                        <div class="row">
                            <div class="col-sm-8">
                                <textarea id="mytextarea" name="post">Write here your post.</textarea>
                            </div>
                            <div class="col-sm-4">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Save post</div>
                                    <div class="panel-body">
                                        <p>Now you can save the post.</p>
                                        <button type="submit" class="btn btn-default btn-sm">Post Now</button>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">Select Category</div>
                                    <div class="panel-body">
                                        <div class="checkbox"><label><input type="checkbox" value="" name="category[]">Category one</label></div>
                                        <div class="checkbox"><label><input type="checkbox" value="" name="category[]">Category two</label></div>
                                        <div class="checkbox"><label><input type="checkbox" value="" name="category[]">Category three</label></div>
                                        <div class="checkbox"><label><input type="checkbox" value="" name="category[]">Category four</label></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('footer.php') ?>