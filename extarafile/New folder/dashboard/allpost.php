<?php
include_once('index.php');
?>
<div class="container-fluid">
    <div class="row">
        <?php include_once('sidebar.php'); ?>
        <div class="col-sm-10">
            <div class="container-fluid">
                <div class="table-panel">
                    <div class="page-header"><h3>All posts</h3></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline">
                                <li><a href="javaScript:;">All (5)</a></li>
                                <li><a href="javaScript:;">Published (5)</a></li>
                                <li><a href="javaScript:;">Trash (5)</a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <table class="table table-responsive table-hover">
                                <tr>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Category</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <td>Lorem Ipsum passage</td>
                                    <td>Admin</td>
                                    <td>Category</td>
                                    <td>Mar 28th, 9:18 am</td>
                                    <td>
                                        <ul class="list-inline">
                                            <li><a href="javaScript:;" class="label label-success">Confirm</a></li>
                                            <li><a href="javaScript:;" class="label label-danger">Disable</a></li>
                                            <li><a href="javaScript:;" class="label label-info">View</a></li>
                                            <li><a href="javaScript:;" class="label label-primary">Edit</a></li>
                                            <li><a href="javaScript:;" class="label label-danger">Trash</a></li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lorem Ipsum passage</td>
                                    <td>Admin</td>
                                    <td>Category</td>
                                    <td>Mar 28th, 9:18 am</td>
                                    <td>
                                        <ul class="list-inline">
                                            <li><a href="javaScript:;" class="label label-success">Confirm</a></li>
                                            <li><a href="javaScript:;" class="label label-danger">Disable</a></li>
                                            <li><a href="javaScript:;" class="label label-info">View</a></li>
                                            <li><a href="javaScript:;" class="label label-primary">Edit</a></li>
                                            <li><a href="javaScript:;" class="label label-danger">Trash</a></li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Lorem Ipsum passage</td>
                                    <td>Admin</td>
                                    <td>Category</td>
                                    <td>Mar 28th, 9:18 am</td>
                                    <td>
                                        <ul class="list-inline">
                                            <li><a href="javaScript:;" class="label label-success">Confirm</a></li>
                                            <li><a href="javaScript:;" class="label label-danger">Disable</a></li>
                                            <li><a href="javaScript:;" class="label label-info">View</a></li>
                                            <li><a href="javaScript:;" class="label label-primary">Edit</a></li>
                                            <li><a href="javaScript:;" class="label label-danger">Trash</a></li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <ul class="list-inline">
                                <li><a href="javaScript:;">All (5)</a></li>
                                <li><a href="javaScript:;">Published (5)</a></li>
                                <li><a href="javaScript:;">Trash (5)</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('footer.php') ?>