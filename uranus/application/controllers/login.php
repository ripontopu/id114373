<?php


class Login extends CI_Controller{
    public function index(){
        $this->load->view('login_page');
    }
    public function user_login_data_check() {
        $this->form_validation->set_rules('username','User Name','trim|xss_clean|min_length[3]');
        $this->form_validation->set_rules('password','Password','trim|xss_clean');
        
        if($this->form_validation->run()==FALSE){
             $this->load->view('login_page');
        }
        else{
            $this->load->model('user_model');  // connect model
            $result = $this->user_model->user_login_data_check();
            echo $result;
            if($result){
                redirect('dashboard');
            }
            else{
                $data['errorLogin'] = 'You are Good';
                $this->load->view('login_page', $data);
            }
        }
    }
}
