<?php
include_once './navigation.php';
include_once './vendor/autoload.php';

use App\BITM\SEIP108594\users\Users;

//session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
$store = new Users();
$allUsers = $store->index();
?>
<html>
    <head>
    </head>
    <body>
        <br><br><br>
        <div class="container">
          <div class="panel panel-primary">
              <div class="panel-heading">Inactive User List</div>
              <table class="table table-striped table-hover">
                  <tr class="info">
                    <th>SL</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                <?php
                if (isset($allUsers) && !empty($allUsers)) {
                    $serial = 0;
                    foreach ($allUsers as $oneUser) {
                        $serial++
                        ?>
                        <tr>
                            <td><?php echo $serial ?></td>
                            <td><?php echo $oneUser['username']; ?></td>
                            <td><?php echo $oneUser['email']; ?></td>
                            <td>
                                <a href="profile.php?id=<?php echo $oneUser['id'] ?>">Show Details</a> | 
                                <a href="trash.php?id=<?php echo $oneUser['id'] ?>">Delete</a> | 
                                <a href="view/BITM/SEIP108594/users/active.php?id=<?php echo $oneUser['id'] ?>">Activate</a> 
                            </td>
                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td colspan="3">
    <?php echo "Not Available" ?>

                        </td>
                    </tr>
<?php }
?>

            </table>
          </div>
      </div>
    </body>
</html>

