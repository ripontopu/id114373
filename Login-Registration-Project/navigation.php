<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mini Project| User registration</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <a href="index.php" class="navbar-brand">My Website</a>
                <button class="navbar-toggle" data-toggle="collapse" data-target=".navHeaderCollapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse navHeaderCollapse">
                    <ul class="nav navbar-nav navbar-right ">
                        <li role="presentation"><a href="index.php">Home</a></li>
                        <?php if (isset($_SESSION['user_group_id']) && $_SESSION['user_group_id'] == '1') { ?>
                            <li role="presentation"><a href="activated.php">Active User List</a></li>
                            <li role="presentation"><a href="userList.php">Inactive User List</a></li>
                        <?php } else { ?>
                        <?php } ?>
                        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])) { ?>
                            <li role="presentation"><a href="profile.php">User Profile</a></li>
                        <?php } else { ?>
                        <?php } ?>
                        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])) { ?>
                        <?php } else { ?>
                            <li role="presentation" class="navbar-right"><a href="signup.php">Sign UP</a></li>
                        <?php } ?>
                        <?php if (isset($_SESSION['id']) && !empty($_SESSION['id'])) { ?>
                            <li role="presentation" class="navbar-right"><a href="signout.php">Sign Out</a></li>
                        <?php } else { ?>
                            <li role="presentation" class="navbar-right"><a href="signin.php">Sign In</a></li>
                        <?php } ?>
                            
                            
                    </ul>
                </div>
            </div>
        </div>
        <div class="navbar navbar-default navbar-fixed-bottom">
            <div class="container">
                <p class="navbar-text">Site Built By Alamgir Hossain</p>
            </div>
        </div>
    </body>
</html>