<?php
error_reporting(E_ALL ^ E_DEPRECATED);
use Language\id114373\program\Program;

include_once '../../../vendor/autoload.php';

$program = $_POST['program'];

$comma_separated = implode(',', $program);

$_POST['program'] = $comma_separated;


$program = new Program();

$program->prepare($_POST)->update();


