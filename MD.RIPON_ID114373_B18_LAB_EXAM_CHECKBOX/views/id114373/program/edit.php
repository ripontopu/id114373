<?php

error_reporting(E_ALL ^ E_DEPRECATED);
use Language\id114373\program\Program;

include_once '../../../vendor/autoload.php';

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$edititem = new Program();

$editoneitem = $edititem->prepare($_GET)->edit();



?>


<a href="index.php">Back to Program list</a>
<fieldset>
    <legend>Your Favourite Programming </legend>
    
    <form action="update.php" method="POST">
        <input type="checkbox" name="program[]" value="C"<?php if(preg_match("/C/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>C<br>
        <input type="checkbox" name="program[]" value="C++"<?php if(preg_match("/C++/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>C++<br>
        <input type="checkbox" name="program[]" value="PHP"<?php if(preg_match("/PHP/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>PHP<br>
        <input type="checkbox" name="program[]" value="C#"<?php if(preg_match("/C#/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>C#<br>
        <input type="checkbox" name="program[]" value="Parl"<?php if(preg_match("/Parl/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>Parl<br>
        <input type="checkbox" name="program[]" value="Pythan"<?php if(preg_match("/Pythan/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>Pythan<br>
        <input type="checkbox" name="program[]" value="Java"<?php if(preg_match("/Java/",$editoneitem['program'])){ echo 'checked';} else{echo"";} ?>>Java<br>
        <input type="submit" value="Update">
       <input type="hidden" name="id" value="<?php echo $editoneitem['id']; ?>">
        
    </form>
</fieldset>
