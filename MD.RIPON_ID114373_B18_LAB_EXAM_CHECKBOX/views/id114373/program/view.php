<?php
error_reporting(E_ALL ^ E_DEPRECATED);
use Language\id114373\program\Program;

include_once '../../../vendor/autoload.php';

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$id =$_GET['id'];

$showitem = new Program();

$showoneitem = $showitem->show($id);


?>
<a href="create.php">Input New Program</a>  |  <a href="index.php">Back to All Program</a>
<fieldset>
    <legend>Hobby</legend>
    <table border="1">
              <tr>
                     <th>ID</th>
                     <th>Programming Language</th>
                     <th>Action</th>
              </tr> 
              <tr>
                     <td><?php echo $showoneitem['id']; ?></td>
                     <td>
                         <?php
                            if(isset($showoneitem['program']) && !empty($showoneitem['program'])){
                                echo $showoneitem['program'];
                            }
                            else{
                                echo "No Program Available";
                            }
                         ?>
                            
                     </td>
                     <td>
                          <a href="edit.php?id=<?php echo $id; ?>">Edit</a> |
                          <a href="delete.php?id=<?php echo $id; ?>">Delete</a> 
                     </td>
              </tr>
</table>
</fieldset>