<?php
error_reporting(E_ALL ^ E_DEPRECATED);
use Language\id114373\program\Program;

include_once '../../../vendor/autoload.php';

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$programitem = new Program();

$programall = $programitem->allview();


?>

<a href="create.php">Input Your Programming Language</a>

<fieldset>
       <legend>All Programming Language</legend>
              <table border="1">
                     <tr>
                            <th>Serial No</th>
                            <th>Programming Language</th>
                            <th>Action</th>
                     </tr>
                     <?php
                        if(isset($programall) && !empty($programall)){
                            $serial=0;
                            foreach ($programall as $viewall) {
                                $serial++
                         
                     ?>
                     <tr>
                            <td><?php echo $serial; ?></td>
                            <td><?php echo $viewall['program']; ?></td>
                            <td>
                                   <a href="view.php?id=<?php echo $viewall['id']; ?>">View</a>
                                   <a href="edit.php?id=<?php echo $viewall['id']; ?>">Edit</a>
                                   <a href="delete.php?id=<?php echo $viewall['id']; ?>">Delete</a>
                            </td>
                     </tr>
                     <?php
                        }
                        }
                        else{
                     ?>
                     
                     <tr>
                            <td colspan="3">No Available Program </td>
                     </tr>
                     
                     <?php 
                        }
                        
                     ?>
       </table>
</fieldset>