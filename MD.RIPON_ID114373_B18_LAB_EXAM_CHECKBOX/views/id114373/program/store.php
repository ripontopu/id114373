<?php

use Language\id114373\program\Program;

include_once '../../../vendor/autoload.php';

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}



$program = $_POST['program'];

$comma_separated = implode(',', $program);

$_POST['program'] = $comma_separated;



$programitem = new Program();

$programitem->prepare($_POST)->store();



