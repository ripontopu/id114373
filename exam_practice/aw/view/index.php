<?php 
 include_once '../vendor/autoload.php';
 
 use App\User;
 
 $user = new User();
 $allUsers = $user->index();
?>
<!DOCTYPE html>
<html>
       <head>
              <meta charset="utf-8" />
              <title>List of all users</title>
       </head>
       <body>
              <a href="create.php">Add new User</a><br><br>
              <fieldset>
                     <legend>All Users</legend>
                     <table border="1">
                            <tr>
                                   <th>SL NO:</th>
                                   <th>ID</th>
                                   <th>Name</th>
                                   <th>Action</th>
                            </tr>
                            <?php 
                                $slNo = 0;
                                foreach ($allUsers as $singleUser) {
                                    $slNo++;
                            ?>
                            <tr>
                                   <td><?php echo $slNo; ?></td>
                                   <td><?php echo $singleUser['id']; ?></td>
                                   <td><?php echo $singleUser['name']; ?></td>
                                   <td>
                                          <a href="show.php?id=<?php echo $singleUser['id']?>">View</a> |
                                          <a href="edit.php?id=<?php echo $singleUser['id']; ?>">Edit</a> |
                                          <a href="delete.php?id=<?php echo $singleUser['id']; ?>">Delete</a>
                                   </td>
                            </tr>
                            <?php 
                                }
                            ?>
                     </table>
              </fieldset>
       </body>
</html>