<?php
include_once '../vendor/autoload.php';

use App\User;

$user = new User();

$singleUserObject = $user->prepare($_GET)->show();
?>

<!DOCTYPE html>
<html>
       <head>
              <meta charset="utf-8" />
              <title>Showing single user</title>
       </head>
       <body>
              <fieldset>
                     <legend>Single User</legend>
                     <table border="1">
                            <tr>
                                   <th>ID</th>
                                   <th>Name</th>
                                   <th>Action</th>
                            </tr>
                            <tr>
                                   <td><?php echo $singleUserObject->id; ?></td>
                                   <td><?php echo $singleUserObject->name; ?></td>
                                   <td>
                                          <a href="edit.php?id=<?php echo $singleUser['id']; ?>">Edit</a>
                                          <a href="delete.php?id=<?php echo $singleUserObject->id?>">Delete</a>
                                   </td>
                            </tr>
                     </table>
                     <br><br><a href="index.php">See User list</a>
              </fieldset>
       </body>
</html>
