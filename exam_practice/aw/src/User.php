<?php

namespace App;

class User 
{
    public $id = '';
    public $name = '';
    public $mysqli = '';
    
    public function __construct() 
    {
        $con = new \mysqli('localhost', 'root', '', 'php18awolad111646');
        $this->mysqli = $con;
        if ($this->mysqli->connect_error) {
          echo "Error in database connection : (" . $this->mysqli->connect_error . ")";  
        }
    }
    
    public function prepare($data = array())
    {
        if (is_array($data) && array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        
        if (is_array($data) && array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        
        return $this;
    } 
    
    public function store()
    {
        $query = "INSERT INTO users (id, name) VALUES (NULL, '" . $this->name . "')";
        if ($this->mysqli->query($query)) {
            echo "User added successfully";
        } else {
            echo "There were problem while adding user";
        }
    }
    
    public function index()
    {
        $users = array();
        $query = "SELECT * FROM users";
        $result = $this->mysqli->query($query);
        
        while ($row = $result->fetch_assoc()) {
            $users[] = $row;
        }
        
        return $users;
    }  
    
    public function show()
    {
        $query = "SELECT * FROM `users` WHERE id = " . $this->id;
        $result = $this->mysqli->query($query);
       $singleuser = mysqli_fetch_object($result);
       
       return $singleuser;
    }
    
    public function delete()
    {
        $query = "DELETE FROM users WHERE id= " . $this->id;
        if ($this->mysqli->query($query)) {
            echo "User deleted successfully";
        } else {
            echo "Failed to delete user";
        }
    }
    
    public function update(){
    
    }
}
