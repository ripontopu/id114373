<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../src/hobby/Hobby.php';

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


$edititem = new Hobby();

$editoneitem = $edititem->prepare($_GET)->edit();

?>


<fieldset>
    <legend>Edit Your Hobby</legend>
        <form action="update.php" method="POST">
            <input type="checkbox" name="hobby[]" value="Gardening"<?php if(preg_match("/Gardening/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Gardening<br>
            <input type="checkbox" name="hobby[]" value="Fishing"<?php if(preg_match("/Fishing/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Fishing<br>
            <input type="checkbox" name="hobby[]" value="Cricket"<?php if(preg_match("/Cricket/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Cricket<br>
            <input type="checkbox" name="hobby[]" value="Football"<?php if(preg_match("/Football/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Football<br>
            <input type="submit" value="Update">
            <input type="hidden" name="id" value="<?php echo $editoneitem['id']; ?>">
        </form>
   
</fieldset>
