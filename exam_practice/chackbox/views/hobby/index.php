<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../src/hobby/Hobby.php';

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$allhobbis = new Hobby();

$onehobby = $allhobbis->index();

?>


<a href="create.php">Create Hobby</a>

<fieldset>
    <legend>All Hobby List</legend>
    <table border="1">
        <tr>
            <th>Serial No</th>
            <th>Hobbies</th>
            <th>Action</th>
        </tr>
        <?php
            if(isset($onehobby) && !empty($onehobby)){
                $serial = 0;
                
                foreach ($onehobby as $oneitem) {


                    
                    $serial++
                            
                    ?>
                            
        <tr>
            <td><?php echo $serial; ?></td>
            <td><?php echo $oneitem['hobby']; ?></td>
            <td>
                <a href="view.php?id=<?php echo $oneitem['id']; ?>">View</a>
                 <a href="edit.php?id=<?php echo $oneitem['id']; ?>">Edit</a>
                 <a href="delete.php?id=<?php echo $oneitem['id']; ?>">Delete</a>
            </td>
        </tr>
        <?php
          }
            }
            else{
               ?> 
            
        <tr>
            <td colspan="3">No Available Hobby</td>
        </tr>
              
        <?php
        }
        ?>
        
    </table>
</fieldset>
