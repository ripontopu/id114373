<?php
include_once './vendor/autoload.php';

use App\bitm\miniProject;
use App\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
//        print_r($_POST);
        $userName = $_POST['name'];
        $email = $_POST['email'];
        $register = new miniProject();
        $register->setUserName($_POST['name']);
        $Verification = $register->setValues($_POST)->logIn();


        $debug = new utility();
        $debug->debug();
        if ($Verification['username'] == $userName || $Verification['email'] == $email) {
            header('location:registration.php');
        } else {
            $register->setValues($_POST)->register();
        }
        ?>
    </body>
</html>
