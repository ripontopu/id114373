<?php
include_once '../../vendor/autoload.php';

use App\bitm\miniProject;
use App\utility\utility;

session_start();
if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
//    $message = $_SESSION['email'];
//    echo "<script type='text/javascript'>alert('Succesfully');</script>";
    ?>   

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Mini Project</title>
            <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <script src="../../js/bootstrap.js" type="text/javascript"></script>
            <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
            <script src="../../js/npm.js" type="text/javascript"></script>
        </head>
        <body>
            <?php
            $myDataObj = new miniProject();
            $_GET['email'] = $_SESSION['email'];
            $_GET['password'] = $_SESSION['password'];


//            $debug = new utility();
//            $debug->debug($myData);
            include_once '../menu/menu.php';
            if ($_SESSION['email'] == 'sumon@gmail.com') {
                $myData = $myDataObj->index();
                $counter = 0;
                ?>
                <div class="container">
                    <h3 class="text-center">All User Information</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Serial</th>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($myData as $oneData) { ?>
                                        <tr>
                                            <td><?php echo ++$counter; ?></td>
                                            <td><?php echo $oneData['username']; ?></td>
                                            <td><?php echo $oneData['email']; ?></td>
                                            <?php if ($oneData['is_active'] == "1") { ?>
                                                <td><a class="btn btn-danger" href="UserActivity.php?id=<?php echo $oneData['id']; ?>&active=<?php echo $oneData['is_active']; ?>">Disable</a></td>        
                                                <td><a  class="btn btn-success">Enable</a></td>
                                            <?php } else { ?>
                                                <td><a class="btn btn-success" href="UserActivity.php?id=<?php echo $oneData['id']; ?>&active=<?php echo $oneData['is_active']; ?>">Enable</a></td>
                                                <td><a  class="btn btn-danger">Disable</a></td>
                                            <?php }
                                            ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </body>
        </html>
    <?php } ?>

<?php
} else {
    header("location:../../index.php");
}
?>