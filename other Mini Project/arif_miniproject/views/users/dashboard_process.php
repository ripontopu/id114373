<?php
include_once '../../vendor/autoload.php';
use App\users\users;
$log=new users();
$log->prepare1($_POST)->login();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>User profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="../../index.php">Login & Registration</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
          <li class="dropdown active"><a class="dropdown-toggle" data-toggle="dropdown" href=""><?php echo " ".$_SESSION['username']; ?> <strong class="caret"></strong></a>
          		<ul class="dropdown-menu">
          			<li>
                                    <a href="logout.php"> Log out</a>
          				<a href="">Setttings</a>
          			</li>
          		</ul>
          </li>
      </ul>
    </div>
  </div>
</nav>
  
<div class="container-fluid" style="margin-top:50px"> 
	<div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
        <div class="panel-heading">User panel</div>
             <div class="panel-body">
                
                 <form role="form" class="form-horizontal" action="store.php" enctype="multipart/form-data" method="post">
                   <div class="form-group">
                                    <label for="firstname" class="col-sm-3 control-label">First Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First name">
                            </div>
                        </div> 
                     <div class="form-group">
                                    <label for="lastname" class="col-sm-3 control-label">Last Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last name">
                            </div>
                        </div> 
                     <div class="form-group">
                                    <label for="personalphone" class="col-sm-3 control-label">Personal Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="personalphone" id="personalphone" placeholder="Personal Phone Number">
                            </div>
                        </div> 
                     <div class="form-group">
                                    <label for="homephone" class="col-sm-3 control-label">Home Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="homephone" id="homephone" placeholder="Home Phone Number">
                            </div>
                        </div>
                     <div class="form-group">
                                    <label for="officephone" class="col-sm-3 control-label">Office Phone</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="officephone" id="officephone" placeholder="Office Phone Number">
                            </div>
                        </div>
                     <div class="form-group">
                            <label for="currentaddress" class="col-sm-3 control-label">Current Address</label>
                               <div class="col-sm-9">
                                   <textarea rows="5" class="form-control" placeholder="Type here"></textarea>     
                            </div>
                        </div>
                     <div class="form-group">
                            <label for="permanentaddress" class="col-sm-3 control-label">Permanent Address</label>
                               <div class="col-sm-9">
                                   <textarea rows="5" class="form-control" placeholder="Type here"></textarea>     
                            </div>
                        </div>
                     <div class="form-group">
                                    <label for="ProfilePic" class="col-sm-3 control-label">Profile Picture</label>
                                    <div class="col-sm-9">
                                        <input type="file"  name="Profilepic" id="Profilepic">
                            </div>
                        </div>
                      <div class="form-group">
                                    <label  class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <input type="submit" class="btn btn-info" value="Save">
                            </div>
                        </div>
                 </form>
                 </div>
            </div>
  	</div>
    </div>
</body>
</html>

