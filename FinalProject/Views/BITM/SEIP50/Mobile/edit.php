<a href="create.php">Create</a>
<?php
error_reporting(E_ALL ^ E_DEPRECATED);
//include_once"../../../../Src/BITM/SEIP50/Mobile/Mobile.php";
include_once"../../../../vendor/autoload.php";

use App\BITM\SEIP50\Mobile\Mobile;
use App\BITM\SEIP50\Utility\Utility;

session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$id = $_GET['id'];

$Mobiles = new Mobile();

$OneMobile = $Mobiles->show($id);

$dbg = new Utility();
//$dbg->debug($OneMobile);
?>
<html>
    <head>
        <title>Update | Mobile Models</title>
    </head>
    <body>
        <fieldset>
            <legend>
               Update Favorite Mobile Models
            </legend> 
            <form action="update.php" method="POST">
                <label></label><br/>
                <input type="text" name="title" id="title" value="<?php echo $OneMobile['title'] ?>"><br/>
                <input type="submit" value="Update">
                <!--<input type="submit" value="Save & Enter Again">-->
                <input type="hidden" name="id" value="<?php echo $id ?>">
                
            </form>
        </fieldset>
    </body>
</html>