<?php
namespace App\users;
use App\users\Message;
use PDO;
error_reporting(E_ALL ^ E_DEPRECATED);
class users {
    public $id='';
    public $user_group_id='';
    public $unique_id='';
    public $is_admin='';
    public $modified_at='';
    public $created_at='';
    public $deleted_at='';
    public $is_active='';
    public $first_name='';
    public $last_name='';
    public $personal_phone='';
    public $home_phone='';
    public $office_phone='';
    public $current_address='';
    public $permanent_address='';
    public $image='';
    public $username='';
    public $email='';
    public $password='';
    public $hosting='root';
    public $psw='';
    public $conn='';
    
    public function __construct() {
        try {
             $this->conn=new PDO('mysql:host=localhost;dbname=usrreg',  $this->hosting,  $this->psw);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        
        catch (PDOException $e) {
            echo $e->getMessage();
        }

       
    }
    
    public function prepare1($data=array()){
        if (array_key_exists('username',$data)&& !empty($data['username'])){
            $this->username=$data['username'];
        }
        if (array_key_exists('email',$data)&& !empty($data['email'])){
            $this->email=$data['email'];
        }
        if (array_key_exists('password',$data)&& !empty($data['password'])){
            $this->password=$data['password'];
        }
        if (array_key_exists('id',$data)&& !empty($data['id'])){
            $this->id=$data['id'];
        }
         if (array_key_exists('first_name',$data)){
            $this->first_name=$data['first_name'];
        }
        if (array_key_exists('last_name',$data)){
            $this->last_name=$data['last_name'];
        }
        if (array_key_exists('personal_phone',$data)){
            $this->personal_phone=$data['personal_phone'];
        }
        if (array_key_exists('home_phone',$data)){
            $this->home_phone=$data['home_phone'];
        }
        if (array_key_exists('office_phone',$data)){
            $this->office_phone=$data['office_phone'];
        }
        if (array_key_exists('current_address',$data)){
            $this->current_address=$data['current_address'];
        }
        if (array_key_exists('permanent_address',$data)){
            $this->permanent_address=$data['permanent_address'];
        }
         if (array_key_exists('image',$data)){
            $this->image=$data['image'];
        }
        if(array_key_exists('user_group_id', $data)){
            $this->user_group_id=$data['user_group_id'];
        }
        if(array_key_exists('unique_id', $data)){
            $this->unique_id=$data['unique_id'];
        }
        if(array_key_exists('is_admin', $data)){
            $this->is_admin=$data['is_admin'];
        }
        if(array_key_exists('is_active', $data)){
            $this->is_active=$data['is_active'];
        }
        return $this;
    }
    
    public function signup(){
           $stmt="INSERT INTO `users`( `email`,`password`, `username`,`created_at`) VALUES (:email,:password,:username,NOW())";
           $q=  $this->conn->prepare($stmt);
           $q->execute(array(':email'=>  $this->email,':password'=>  $this->password,':username'=>  $this->username));

       if($q){
       Message::message("Registration has been successfully done");
       $url=('../../signup.php');
                header('location:'.$url);
       }
       else{
          Message::message("Error");
       }
       return $this;
    }
    public function store(){
         $q="SELECT MAX(id) FROM `users`";
         $result=$this->conn->query($q);
         $row= $result->fetch(PDO::FETCH_ASSOC);
         
         $foreignkey = $row['MAX(id)']+1;
        $query="INSERT INTO `users`( `email`,`password`, `username`,`created_at`) VALUES (:email,:password,:username,NOW())";
        $row=  $this->conn->prepare($query);
       
         
        $forkey = "INSERT INTO `profiles` (`id`, `user_id`) VALUES (NULL, '".$foreignkey."')";
        $this->conn->query($forkey);
       if($row){
      echo " successfully done";
       
       }
       else{
           echo ('Something is wrong');
       }
     
    }

    public function login(){
            $stm="SELECT * FROM `users` WHERE (username='$this->username' or email='$this->username') and password='$this->password'";
            $res=  $this->conn->query($stm);
            $result=$res->fetch(PDO::FETCH_ASSOC);
            $rows=$res->rowCount();
            
            if($rows==1){
                $_SESSION['username']=':username';
                $_SESSION['id']='$this->id';
                $url=('../../views/users/dashboard_process.php');
                header('location:'.$url);
            }
            else{
                Message::message('Username and Password not found');
            }
            $_SESSION['username']=$result['username'];
            $_SESSION['id']=$result['id'];
            return $result;
    }
    
    
}


