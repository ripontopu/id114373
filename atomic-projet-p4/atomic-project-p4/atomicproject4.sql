-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2016 at 08:43 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject4`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE IF NOT EXISTS `birthdate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `created_at` datetime NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `birthdate`
--

INSERT INTO `birthdate` (`id`, `date`, `created_at`, `delete_at`) VALUES
(19, '2016-03-26', '0000-00-00 00:00:00', NULL),
(20, '2020-03-22', '0000-00-00 00:00:00', NULL),
(21, '2016-03-31', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `created_at`, `delete_at`) VALUES
(17, 'Bangla', '0000-00-00 00:00:00', NULL),
(18, 'English', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city`, `delete_at`) VALUES
(2, 'Sylhet', '2016-03-31 00:00:00'),
(3, 'Dhaka', NULL),
(4, 'Chitagong', NULL),
(5, 'Rajshahi', NULL),
(6, 'Sylhet', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `delete_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `delete_at`) VALUES
(1, 'enripon@gmail.com', NULL),
(2, 'ittopu@gmail.com', NULL),
(3, 'en.ripon@yahoo.com', 2016),
(4, 'template8@webcommander.biz', NULL),
(5, 'en.ripon@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gender` varchar(255) NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender`, `delete_at`) VALUES
(1, 'Male', '2016-03-30 00:00:00'),
(4, 'Male', NULL),
(5, 'Female', NULL),
(6, 'Male', NULL),
(7, 'Female', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hobby` varchar(255) NOT NULL,
  `delete_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobby`, `delete_at`) VALUES
(8, 'Gardening,Fishing,Cricket,Football', NULL),
(10, 'Cricket,Football,Singing', NULL),
(12, 'Cricket,Football', 2016),
(13, 'Football,Singing', NULL),
(14, 'Cricket,Football', 2016),
(15, 'Gardening,Cricket,Football,Singing', NULL),
(16, 'Gardening,Cricket,Football,Singing', NULL),
(17, 'Cricket,Football', 2016),
(19, 'Gardening,Cricket,Football', NULL),
(20, 'Cricket,Football,Singing', NULL),
(21, 'Cricket,Football,Singing', NULL),
(22, 'Cricket,Football,Singing', NULL),
(23, 'Cricket,Football', NULL),
(24, 'Cricket,Football', 2016),
(25, 'Cricket,Football', 2016),
(26, 'Cricket,Football', 2016),
(27, 'Gardening,Fishing', 2016);

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE IF NOT EXISTS `mobile` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `edit` int(11) NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `title`, `edit`, `delete_at`) VALUES
(15, 'Nokia', 0, '0000-00-00 00:00:00'),
(17, 'Symphony H175', 0, NULL),
(18, 'Walton GH5', 0, NULL),
(19, 'Samsung', 0, NULL),
(20, 'Sony', 0, NULL),
(21, 'HTC', 0, NULL),
(22, 'iPhone', 0, NULL),
(23, 'Samsung J5', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE IF NOT EXISTS `picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`id`, `profile_name`, `image`, `delete_at`, `active`) VALUES
(12, '', '1459967988IMG_7138.JPG', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profileimage`
--

CREATE TABLE IF NOT EXISTS `profileimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `delete_at` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `profileimage`
--

INSERT INTO `profileimage` (`id`, `profile_name`, `image`, `delete_at`, `active`) VALUES
(5, 'monwar', '1459766748Ripon cartoon 1.jpg', NULL, 1),
(7, 'info', '1459766813Ripon cartoon 1.jpg', '2016-04-04 00:00:00', 0),
(8, 'afasdfa', '1459932477IMG_7093.JPG', NULL, 0),
(9, 'topu', '1459768139IMG_7093.JPG', NULL, 0),
(10, 'Md. Ripon Miah', '1459768155IMG_7093.JPG', NULL, 0),
(11, 'infollllll', '1459790928IMG_7138.JPG', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) NOT NULL,
  `profile_photo` varchar(255) NOT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_name`, `profile_photo`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 'Emon', '1459459026Lighthouse.jpg', NULL, '2016-03-31 10:32:54', '2016-03-31 10:32:54', NULL),
(22, 'Mesbaul Islam', '1459941383Lighthouse.jpg', 1, '2016-03-31 10:33:30', '2016-03-31 10:33:30', NULL),
(23, 'Mesbaul Islam', '1459941364Tulips.jpg', NULL, '2016-03-31 10:34:40', '2016-03-31 10:34:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE IF NOT EXISTS `summary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `summary` varchar(255) NOT NULL,
  `delete_at` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `summary`, `delete_at`) VALUES
(12, 'another sample text and also this is test so you cen enjoy this tutorial', NULL),
(13, 'only sample text', 2016),
(14, 'another sample text', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
