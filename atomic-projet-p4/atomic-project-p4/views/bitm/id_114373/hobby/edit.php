<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\bitm\id_114373\hobby\Hobby;

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

//$id =$_GET['id'];

$edititem = new Hobby();

$editoneitem = $edititem->prepare($_GET)->edit();

?>
<html>
       <head>
              <title>Hobby</title>
              <style>
                     body{text-align: center}
                .menu a {
  background: #ddd none repeat scroll 0 0;
  color: #000;
  margin-right: 10px;
  padding: 10px;
  text-decoration: none;
  text-transform: uppercase;
}
.menu a:hover{
    background: gold;
    text-shadow:1px 1px 1px;
    
}
              </style>
       </head>
<br><br>
<div class="menu">
    <a href="../mobile/index.php">Mobile</a>
<a href="../book/index.php">Book</a>
<a href="../birthdath/index.php">Birthdate</a>
<a href="index.php">Hobby</a>
<a href="../summary/index.php">Summary</a>
<a href="../gender/index.php">Gender</a>
<a href="../email/index.php">Email</a>
<a href="../city/index.php">City</a>
<a href="../picture/index.php">Profile Picture</a>
</div>
</br></br>
 <a href="create.php">Crete Hobby</a>  |  <a href="index.php">All Hobby</a>
<fieldset>
    <legend>Edit Your Hobby</legend>
        <form action="update.php" method="POST">
            <input type="checkbox" name="hobby[]" value="Gardening"<?php if(preg_match("/Gardening/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Gardening<br>
            <input type="checkbox" name="hobby[]" value="Fishing"<?php if(preg_match("/Fishing/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Fishing<br>
            <input type="checkbox" name="hobby[]" value="Cricket"<?php if(preg_match("/Cricket/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Cricket<br>
            <input type="checkbox" name="hobby[]" value="Football"<?php if(preg_match("/Football/",$editoneitem['hobby'])){ echo 'checked';} else{echo"";} ?>>Football<br>
            <input type="submit" value="Update">
            <input type="hidden" name="id" value="<?php echo $editoneitem['id']; ?>">
        </form>
   
</fieldset>
</html>