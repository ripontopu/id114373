<?php

error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\bitm\id_114373\gender\Gender;

session_start();

if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$id = $_GET['id'];

$deleted = new Gender();

$deleted->trash($id);