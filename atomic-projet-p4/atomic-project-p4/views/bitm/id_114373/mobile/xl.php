<?php
error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
include_once'../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';

use App\bitm\id_114373\mobile\Mobile;



$showall = new Mobile();

$modelitem = $showall->index();



//Create new PHPExcel object

$objPHPExcel = new PHPExcel();

// Set document Propertise

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Marten balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test rusult file");


// Add some Data

$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','SL')
        ->setCellValue('B1','ID')
        ->setCellValue('C1','Title');
$counter = 2;
$serial = 0;


foreach ($modelitem as $oneitem) {
    $serial++;
    $objPHPExcel->setActivesheetIndex(0)
        ->setCellValue('A'.$counter,  $serial)
        ->setCellValue('B'.$counter, $oneitem['id'])
        ->setCellValue('C'.$counter, $oneitem['title']);
           $counter++;
}

// Rename Worksheet
$objPHPExcel->getActiveSheet()->setTitle('Mobile_list');

// Set active sheet index to the first sheet, so excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect Output to a client's web Browser excels
header('Content-Type:application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="01simple.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' .gmdate('D, d M Y H:i:s').'GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
