<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\bitm\id_114373\mobile\Mobile;



$showall = new Mobile();

$modelitem = $showall->index();

$trs ='';
$serial=0;
foreach ($modelitem as $oneitem ):
$serial++;
$trs.="<tr>";
$trs.="<td>".$serial."</td>";
$trs.="<td>".$oneitem['id']."</td>";
$trs.="<td>".$oneitem['title']."</td>";
$trs.="</tr>";
endforeach;

$html = <<<EOD
        
        <!DOCTYPE html>
<html>
    <head>
        <title>List Of  Mobile Model</title>
   </head>
        <body>
            <h1>List Of Mobiles</h1>
            <table border="1">
                   <thead>
                          <tr>
                                 <th>Serial</th>
                                  <th>ID</th>
                                   <th>Model</th>
                          </tr>
                   </thead>
                   <tbody>
                       $trs;
                   </tbody>
            </table>
       </body>
        </html>
EOD;

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'enripon@gmail.com';                 // SMTP username
$mail->Password = 'password';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('enripon@gmail.com', 'Mailer');
$mail->addAddress('cse.ripon63@gmail.com', 'Joe User');     // Add a recipient
$mail->addAddress('ellen@example.com');               // Name is optional
$mail->addReplyTo('info@example.com', 'Information');
$mail->addCC('cc@example.com');
$mail->addBCC('bcc@example.com');

//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Here is the subject';
$mail->Body    = 'This is the HTML message body <b>in bold!</b>';
$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent';
}
