<?php

namespace App\bitm\id_114373\picture;
use PDO;
class Picture {
    public $title = '';
    public $id = '';
    public $user_name ='';
    public  $image_name = '';
    public $connect= '';
    public $password ='';
    public $username= 'root';
    


    public function prepare($data= '') {
        if(array_key_exists('name', $data) && !empty($data['name'])){
            $this->user_name= $data['name'];
            //echo $this->title;
        }
        if(array_key_exists('image', $data) && !empty($data['image'])){
            $this->image_name=$data['image'];
        }
        if(array_key_exists('id', $data) && !empty($data['id'])){
            $this->id= $data['id'];
        }
        if(array_key_exists('active', $data) && !empty($data['active'])){
            $this->id= $data['active'];
        }
        return $this;
    }
    
    public function __construct() {
        try {
            $this->connect = new PDO ("mysql:host=localhost; dbname=atomicproject4",$this->username, $this->password);
            $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }

    }
    
    public function store() {
        session_start();
    $query = "INSERT INTO `atomicproject4`.`picture` (`id`, `profile_name`, `image`) "
            . "VALUES (NULL, :name,:photo)";
    $storeid=$this->connect->prepare($query);
    $storeid->bindParam(":name", $this->user_name);
    $storeid->bindParam(":photo", $this->image_name);
    $storeid->execute();
         $_SESSION['Message']="Uplode Successfully";
        header('location:index.php');
    }
    
    public function index() {
       
        try {
            $mypicture = array();
        $query ="SELECT * FROM `picture` WHERE delete_at IS NULL";
        $allshow= $this->connect->query($query) or die("failed");
        while ($row = $allshow->fetch(PDO::FETCH_ASSOC)){
            $mypicture[]=$row;
        }
        return $mypicture;
            
        } catch (PDOException $exc) {
            echo $exc->getMessge();
        }
   
    }
   
    public function show($id = '') {
        try {
              $this->id = $id;
        //echo $this->id;
        $query = "SELECT * FROM `picture` WHERE id=:id";
        $oneshow = $this->connect->prepare($query) or die('Connect Failed');
        $oneshow->execute(array(':id'=>  $this->id));
        $onepicture=$oneshow->fetch(PDO::FETCH_ASSOC);
     return $onepicture;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
 
    }
    
    
    public function delete($id='') {
        try {
        $this->id=$id;
        $delete_query = "SELECT * FROM picture WHERE id = $this->id";
        $deleteitem = $this->connect->query($delete_query) or die('Failed');
        $row = $deleteitem->fetch(PDO::FETCH_ASSOC);
        if(array_key_exists('image', $row)){
            $delete_query2 = "DELETE FROM picture WHERE active IS NULL AND id = ".$row['id'];
            $deleteitem2 = $this->connect->prepare($delete_query2);
            $deleteitem2->execute();
            if($delete_query2){
                    unlink("../../../../img/".$row['image']);
            }
        $_SESSION['Message']="Permanently Delete Profile Picture";
        header('location:index.php');
        
        }
        
       
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
        
        

    }
    
    public function trash($id=''){
        
        try {
        $this->id=$id;
        $query = "UPDATE `atomicproject4`.`picture` SET `delete_at` = '".date('Y-m-d')."' WHERE `picture`.`id` = :id";
        $trash = $this->connect->prepare($query);
        $trash->execute(array(':id'=>$this->id));
       
        $_SESSION['Message'] = "Delete Successfully";
        header('location:index.php');
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }

                
    }
    
    public function trashted() {
        
        try {
            
        $myprofile = array();
        
        $query ="SELECT * FROM `picture` WHERE delete_at IS NOT NULL";
        $alldelete=$this->connect->prepare($query) or die('Faield');
        $alldelete->execute();
        while($row = $alldelete->fetch(PDO::FETCH_ASSOC)){
            $myprofile[]= $row;
        }
        return $myprofile;
            
            
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }

    }


    
    public function update() {
        
        try {
            
            if(isset($this->image_name) &&  !empty($this->image_name)){
            $query = "UPDATE `atomicproject4`.`picture` SET `profile_name` = :name, `image` = :image WHERE `picture`.`id` =:id";
            $update = $this->connect->prepare($query);
            $update->execute(array(':name'=> $this->user_name, ':image'=> $this->image_name, ':id'=>  $this->id));
        }
        else{
            $query = "UPDATE `atomicproject4`.`picture` SET `profile_name` =:name "
                    . "WHERE `picture`.`id` =:id";
            $update= $this->connect->prepare($query);
            $update->execute(array(':name'=>  $this->user_name, ':id'=>  $this->id));
             $_SESSION['Message']="Update Successfull";
        
        }
        header('location:index.php');
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }


    }
    
    public function active() {
        try {
            
            $query1 = "UPDATE picture SET active = :set_as_null";
            $query2 = "UPDATE picture SET active = :set_as_one WHERE id = $this->id";
            $active1 = $this->conn->prepare($query1);
            $active2 = $this->conn->prepare($query2);
            $active1->execute(array(':set_as_null'=>NULL));
            $active2->execute(array(':set_as_one'=>1));
            header('location:active.php');
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
        }
    
  
    
    
}
