<?php

include_once '../src/Gender.php';

session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$id = $_GET['id'];

$genderone= new Gender();

$showgender = $genderone->show($id);

?>
<a href="index.php">All Gender</a>
<fieldset>
    <legend>Gender</legend>
    <table border="1">
        <tr>
            <th>ID</th>
            <th>Gender</th>
            <th>Action</th>
        </tr>
        <tr>
            <td><?php echo $showgender['id']; ?></td>
             <td><?php 
                if(isset($showgender['gender']) && !empty($showgender['gender'])){
                    echo $showgender['gender']; 
                }
             ?></td>
             <td>
                 <a href="edit.php?id=<?php echo $showgender['id']; ?>">Edit</a>
                 <a href="delete.php?id=<?php echo $showgender['id']; ?>">Delete</a>
             </td>
        </tr>
    </table>
</fieldset>
