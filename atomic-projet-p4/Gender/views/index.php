<?php

include_once '../src/Gender.php';

session_start();
if(isset($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$genderall= new Gender();

$showallgender = $genderall->index();


?>
<a href="create.php">Gender Creation</a>
<fieldset>
    <legend>All Gender</legend>
    <table border="1">
        <tr>
            <th>Serial</th>
              <th>Gender</th>
                <th>Action</th>
        </tr>
        <?php  
            if(isset($showallgender) && !empty($showallgender)){
                $serial =0;
                foreach ($showallgender as $genderitem) {
                    $serial++
            
        ?>
        
        <tr>
            <td><?php echo $serial; ?></td>
             <td><?php echo $genderitem['gender']; ?></td>
             <td>
                 <a href="view.php?id=<?php echo $genderitem['id']; ?>">View</a>
                 <a href="edit.php?id=<?php echo $genderitem['id']; ?>">Edit</a>
                 <a href="delete.php?id=<?php echo $genderitem['id']; ?>">Delete</a>
             </td>
        </tr>
        <?php 
            }
            }
            else{
        
        ?>
        
        <tr>
            <td colspan="3"><?php echo "No Gender Available"; ?></td>
        </tr>
        <?php
        }
        ?>
    </table>
</fieldset>